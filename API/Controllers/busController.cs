﻿using Data.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class busController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage busInfo(bususer b)
        {
            if (b.userId.Equals("123"))
            {
                busOperation bo = new busOperation();
                var result = bo.GetBusDetails();
                List<string> listName = new List<string>();
                string s = "";

                foreach (var i in result)
                {
                    listName.Add(Convert.ToString(i.busId));
                }
                int index;
                for (index = 0; index < listName.Count - 1; index++)
                {
                    s += listName[index];
                    s += "%";
                }
                s += listName[index];

                return Request.CreateResponse(HttpStatusCode.OK, s);
            }
            else {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "bad request");
            }
        }
    }
    public class bususer
    {
        public string userId { get; set; }
    }
}
