﻿using Data;
using Data.DTO;
using Data.Entities;
using Data.Operations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    
    public class updateController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage updatelocation(updainfo info)
        {
            int b = Convert.ToInt32(info.busId);

            double lt = Convert.ToDouble(info.lat);
            double ln = Convert.ToDouble(info.lon);
            dataContext dc = new dataContext();
            locationTable data = new locationTable();
            data = dc.locationTable.FirstOrDefault(x => x.busId == b);
            if (data != null)
            {
                data.latitude = lt;
                data.longitude = ln;
                data.userId = info.userId;
                data.dateTime = DateTime.Now;
                try
                {
                    dc.locationTable.Attach(data);
                    dc.Entry(data).State = EntityState.Modified;
                    dc.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, "success");
                }
                catch (Exception e)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "data Null");
                }
            }
            else
            {
                try
                {
                    locationTable data2 = new locationTable();
                    data2.busId = b;
                    data2.latitude = lt;
                    data2.userId = info.userId;
                    data2.longitude = ln;
                    data2.dateTime = DateTime.Now;
                    //locationOperation oper = new locationOperation();
                    /*var s = oper.addLocationDetails(data2);
                    if (s == true)
                        return Request.CreateResponse(HttpStatusCode.OK, "success");
                    else
                        return Request.CreateErrorResponse(HttpStatusCode.NotAcceptable, "not success");*/
                    dc.locationTable.Add(data2);
                    dc.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK,"success");

                }
                catch (Exception e)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Insertion wrong");
                }
            }

        }

    }
    public class updainfo
    {
        public string busId { get; set; }
        public string lat { get; set; }
        public string lon { get; set; }
        public string userId { get; set; }
    }
}

