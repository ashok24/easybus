﻿using Data;
using Data.DTO;
using Data.Entities;
using Data.Operations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class driverController : ApiController
    {
        
        [HttpPost]
        public HttpResponseMessage driverlogin(Login login)
        {
            userTable dto = new userTable();
            dto = null;

            dataContext dc = new dataContext();

            var data = dc.userTable.Where(x => x.userName == login.userName && x.password == login.password).ToArray();
            dto = dc.userTable.FirstOrDefault(x => x.userName == login.userName && x.password == login.password);
            if (dto != null)
            {
                if (data.Any(u => u.userName == login.userName && u.password == login.password && u.roleId == 2))
                {
                    return Request.CreateResponse(HttpStatusCode.OK, dto.userId);
                }
                else
                {

                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Not Valid");
                }
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "not found");
            }
        }
    }
    public class Login
    {
        public string userName { get; set; }
        public string password { get; set; }
    }

}

