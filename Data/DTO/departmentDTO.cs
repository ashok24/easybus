﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.DTO
{
    public class departmentDTO
    {
        public int deptno { get; set; }
        public string deptname { get; set; }
    }
}
