﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.DTO
{
    public class routeDTO
    {
        public int routeId { get; set; }
        public string destination { get; set; }
        public string via { get; set; }
    }
}
