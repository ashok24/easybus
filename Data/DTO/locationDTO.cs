﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.DTO
{
    public class locationDTO
    {
       
        public int busId { get; set; }
        public string userId { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public DateTime dateTime { get; set; }
    }
}
