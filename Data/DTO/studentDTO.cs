﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.DTO
{
    public class studentDTO
    {
        public string userId { get; set; }
        public int busId { get; set; }
        public string studentName { get; set; }
        public int departmentId { get; set; }
        public string parentName { get; set; }
        public string phoneNumber { get; set; }
    }
}
