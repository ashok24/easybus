﻿using Data.DTO;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Operations
{
    public class routeOperation
    {
            dataContext db = new dataContext();
            public Boolean Add(routeDTO model)
            {
                routeTable rt = new routeTable { routeId = model.routeId, destination = model.destination, via = model.via };
                db.routeTable.Add(rt);
                db.SaveChanges();
                return true;
            }
            public IList<routeDTO> Display()
            {
                IList<routeDTO> ilist = db.routeTable.Select(s => new routeDTO()
                {
                    routeId = s.routeId,
                    destination = s.destination,
                    via = s.via


                }).ToList();
                return ilist;
            }
            public bool delete(int routeId)
            {

                var del = db.routeTable.FirstOrDefault(x => x.routeId == routeId);
                db.routeTable.Remove(del);
                db.SaveChanges();
                return true;
            }
            public routeDTO Editroute(int routeId)
            {
                var s = db.routeTable.FirstOrDefault(x => x.routeId == routeId);
                routeDTO dto = new routeDTO();
                dto.routeId = s.routeId;
                dto.destination = s.destination;
                dto.via = s.via;
                return dto;

            }
            public bool updateroute(routeDTO dto)
            {
                routeTable r = new routeTable() { routeId = dto.routeId, destination = dto.destination, via = dto.via };
                db.routeTable.Attach(r);
                db.Entry(r).State = EntityState.Modified;
                db.SaveChanges();
                return true;


            }
        }
    }