﻿using Data.DTO;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Operations
{
    public class studentOperation
    {
       
            dataContext db = new dataContext();

            public bool addstudentTable(studentDTO model)
            {
            try
            {
                studentTable record = new studentTable { userId = model.userId, busId = model.busId, departmentId = model.departmentId, parentName = model.parentName, phoneNumber = model.phoneNumber, studentName = model.studentName };
                db.studentTable.Add(record);
                db.SaveChanges();
                return true;
            }
            catch(Exception err)
            {
                return false;
            }
     }
            public bool edit(studentDTO model)
            {
            try
            {
                studentTable record = new studentTable { userId = model.userId, busId = model.busId, departmentId = model.departmentId, parentName = model.parentName, phoneNumber = model.phoneNumber, studentName = model.studentName };
                db.studentTable.Attach(record);
                db.Entry(record).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch(Exception err)
            {
                return false;
            }
            }

            public IList<studentDTO> viewstudentDetails()
            {

                IList<studentDTO> students = db.studentTable.Select(s => new studentDTO()
                {

                    userId = s.userId,
                    busId = s.busId,
                    departmentId = s.departmentId,
                    parentName = s.parentName,
                    studentName = s.studentName,
                    phoneNumber = s.phoneNumber
                }).ToList();

                return students;
            }

        public bool deletestudentDetails(string userId)
        {
            try
            {
                var delete = db.studentTable.FirstOrDefault(x => x.userId == userId);
                db.studentTable.Remove(delete);
                db.SaveChanges();
                return true;
            }
            catch(Exception err)
            {
                return false;
            }
        }

            public studentDTO editstudentdetails(string userId)
            {
                var edit = db.studentTable.FirstOrDefault(x => x.userId == userId);
                var editdetails = new studentDTO()
                {
                    userId = edit.userId,
                    studentName = edit.studentName,
                    parentName = edit.parentName,
                    phoneNumber = edit.phoneNumber,
                    busId = edit.busId,
                    departmentId = edit.departmentId
                };
                return editdetails;

            }
        }
    }