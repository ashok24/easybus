﻿using Data.DTO;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Operations
{
    public class departmentOperation
    {
        dataContext dc = new dataContext();
        public bool savedepartmentTable(departmentDTO model)
        {

            departmentTable result = new departmentTable() { deptno = model.deptno, deptname = model.deptname };
            dc.departmentTable.Add(result);
            dc.SaveChanges();
            return true;
        }
        public IEnumerable<departmentDTO> GetAlldepartment()
        {
            IEnumerable<departmentDTO> result = dc.departmentTable.Select(s => new departmentDTO()
            {
                deptno = s.deptno,
                deptname = s.deptname
            }).ToList();

            return result;
        }
        public bool deletedepartmentTable(int deptno)
        {
            var rt = dc.departmentTable.FirstOrDefault(x => x.deptno == deptno);
            dc.departmentTable.Remove(rt);
            dc.SaveChanges();
            return true;
        }
        public bool editdepartmentTable(int deptno)
        {

            return true;
        }
    }
}
