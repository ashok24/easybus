﻿using Data.DTO;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Operations
{
    public class roleOperation
    {
        dataContext dc = new dataContext();
        public IEnumerable<roleDTO> GetAllrole()
        {
            IEnumerable<roleDTO> result = dc.roleTable.Select(s => new roleDTO()
            {
                roleno = s.roleno,
                rolename = s.rolename
            }).ToList();

            return result;
        }
        public bool saveroleTable(roleDTO model)
        {

            roleTable result = new roleTable() { roleno = model.roleno, rolename = model.rolename };
            dc.roleTable.Add(result);
           dc.SaveChanges();
            return true;
        }

        public bool deleteroleTable(int roleno)
        {
            var rt = dc.roleTable.FirstOrDefault(x => x.roleno == roleno);
            dc.roleTable.Remove(rt);
            dc.SaveChanges();
            return true;
        }
        public bool editroleTable(int roleno)
        {
            return true;
        }

    }
}
