﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class departmentTable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int deptno { get; set; }
        [DataType("varchar(20)")]
        public string deptname { get; set; }
    }
}
