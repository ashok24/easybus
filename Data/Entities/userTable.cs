﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class userTable
    {
        [Key]
        public string userId { get; set; }
        [DataType("varchar(20)")]
        public string userName { get; set; }
        [DataType("varchar(20)")]
        public string firstName { get; set; }
        [DataType("varchar(20)")]
        public string password { get; set; }
        [DataType("integer")]
        public int roleId { get; set; }

        [ForeignKey("roleId")]
        public virtual roleTable roleTable { get; set; }

    }
}
