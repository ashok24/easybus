﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class roleTable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [DataType("integer")]
        public int roleno { get; set; }
        [DataType("varchar(20)")]
        public string rolename { get; set; }
    }
}
