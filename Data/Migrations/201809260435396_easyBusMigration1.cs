namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class easyBusMigration1 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.busTables", "userId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.busTables", "userId", c => c.String());
        }
    }
}
