namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class easyBusMigration3 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.locationTables");
            AlterColumn("dbo.locationTables", "locationId", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.locationTables", "userId", c => c.String(maxLength: 128));
            AddPrimaryKey("dbo.locationTables", "locationId");
            CreateIndex("dbo.busTables", "routeId");
            CreateIndex("dbo.locationTables", "busId");
            CreateIndex("dbo.locationTables", "userId");
            CreateIndex("dbo.userTables", "roleId");
            CreateIndex("dbo.studentTables", "busId");
            CreateIndex("dbo.studentTables", "departmentId");
            AddForeignKey("dbo.busTables", "routeId", "dbo.routeTables", "routeId", cascadeDelete: true);
            AddForeignKey("dbo.locationTables", "busId", "dbo.busTables", "busId", cascadeDelete: true);
            AddForeignKey("dbo.userTables", "roleId", "dbo.roleTables", "roleno", cascadeDelete: true);
            AddForeignKey("dbo.locationTables", "userId", "dbo.userTables", "userId");
            AddForeignKey("dbo.studentTables", "busId", "dbo.busTables", "busId", cascadeDelete: true);
            AddForeignKey("dbo.studentTables", "departmentId", "dbo.departmentTables", "deptno", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.studentTables", "departmentId", "dbo.departmentTables");
            DropForeignKey("dbo.studentTables", "busId", "dbo.busTables");
            DropForeignKey("dbo.locationTables", "userId", "dbo.userTables");
            DropForeignKey("dbo.userTables", "roleId", "dbo.roleTables");
            DropForeignKey("dbo.locationTables", "busId", "dbo.busTables");
            DropForeignKey("dbo.busTables", "routeId", "dbo.routeTables");
            DropIndex("dbo.studentTables", new[] { "departmentId" });
            DropIndex("dbo.studentTables", new[] { "busId" });
            DropIndex("dbo.userTables", new[] { "roleId" });
            DropIndex("dbo.locationTables", new[] { "userId" });
            DropIndex("dbo.locationTables", new[] { "busId" });
            DropIndex("dbo.busTables", new[] { "routeId" });
            DropPrimaryKey("dbo.locationTables");
            AlterColumn("dbo.locationTables", "userId", c => c.String());
            AlterColumn("dbo.locationTables", "locationId", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.locationTables", "locationId");
        }
    }
}
