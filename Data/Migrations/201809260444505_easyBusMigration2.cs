namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class easyBusMigration2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.locationTables", "userId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.locationTables", "userId");
        }
    }
}
