namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class easyBusMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.busTables",
                c => new
                    {
                        busId = c.Int(nullable: false),
                        busNo = c.String(nullable: false, maxLength: 12),
                        userId = c.String(),
                        routeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.busId);
            
            CreateTable(
                "dbo.departmentTables",
                c => new
                    {
                        deptno = c.Int(nullable: false),
                        deptname = c.String(),
                    })
                .PrimaryKey(t => t.deptno);
            
            CreateTable(
                "dbo.locationTables",
                c => new
                    {
                        locationId = c.Int(nullable: false),
                        busId = c.Int(nullable: false),
                        latitude = c.Double(nullable: false),
                        longitude = c.Double(nullable: false),
                        dateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.locationId);
            
            CreateTable(
                "dbo.roleTables",
                c => new
                    {
                        roleno = c.Int(nullable: false),
                        rolename = c.String(),
                    })
                .PrimaryKey(t => t.roleno);
            
            CreateTable(
                "dbo.routeTables",
                c => new
                    {
                        routeId = c.Int(nullable: false),
                        destination = c.String(),
                        via = c.String(),
                    })
                .PrimaryKey(t => t.routeId);
            
            CreateTable(
                "dbo.studentTables",
                c => new
                    {
                        userId = c.String(nullable: false, maxLength: 128),
                        busId = c.Int(nullable: false),
                        studentName = c.String(nullable: false),
                        departmentId = c.Int(nullable: false),
                        parentName = c.String(nullable: false),
                        phoneNumber = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.userId);
            
            CreateTable(
                "dbo.userTables",
                c => new
                    {
                        userId = c.String(nullable: false, maxLength: 128),
                        userName = c.String(),
                        firstName = c.String(),
                        password = c.String(),
                        roleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.userId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.userTables");
            DropTable("dbo.studentTables");
            DropTable("dbo.routeTables");
            DropTable("dbo.roleTables");
            DropTable("dbo.locationTables");
            DropTable("dbo.departmentTables");
            DropTable("dbo.busTables");
        }
    }
}
